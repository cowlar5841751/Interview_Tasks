# Table of Contents

1. [Car Price Prediction](#car-price-prediction)
   - [Dataset Overview](#dataset-overview)
   - [Approach](#approach)
   - [Key Insights](#key-insights)
   - [Model Development](#model-development)
   - [Data Preprocessing](#data-preprocessing)
   - [Regression Algorithm](#regression-algorithm)
   - [Challenges](#challenges)

2. [Movie Recommendation System](#movie-recommendation-system)
   - [Dataset Overview](#dataset-overview-1)
   - [Approach](#approach-1)
   - [Key Components](#key-components)
   - [Implementation](#implementation)
   - [Challenges](#challenges-1)

---

## Car Price Prediction

### Dataset Overview
The dataset, named "cars_data.csv," contains detailed information about various cars, including their make, model, version, price, make year, engine capacity (CC), assembly, mileage, registered city, and transmission.

### Approach
The objective was to develop an accurate machine learning model for predicting car prices based on their features. The dataset underwent data cleaning and feature engineering to optimize the model. Various regression models, including MLP Regression and Random Forest, were applied to address the non-linearity in the dataset.

### Key Insights
The dataset includes columns such as Make, Model, Version, Price, Make_Year, CC, Assembly, Mileage, Registered City, and Transmission.

### Model Development
The following regression algorithms were applied:

- Random Forest Regressor (MSE: 0.05089615861063002)
- MultiLayer Perceptron (MLP) Regressor (MSE: 0.17064087738626135)

Random Forest Regression performed well on this problem.

### Data Preprocessing
Preprocessing steps included handling missing values, encoding categorical variables, and scaling numerical features using the Standard Scalar technique.

### Regression Algorithm
Random Forest Regression

### Challenges
- Some entries lacked respective labels.
- Encoding categorical variables into numerical values posed challenges.

---

## Movie Recommendation System

### Dataset Overview
The dataset comprises user ratings for various movies, structured as a user-item matrix where rows represent movies, and columns represent user ratings on a scale of 0 to 5.

### Approach
The system suggests movies based on the correlation between user preferences, with higher correlation implying stronger recommendations. This correlation was found using cosine similarity.

### Key Components
- **Cosine Similarity:** Helped to find movie reccomendations based on user ratings of similar users.
- **Movie Recommendation Function:** Developed a function for personalized movie recommendations.

### Implementation
The implementation involved loading the movie ratings dataset, calculating the cosine similarity and creating a function for movie recommendations based on user preferences.

### Challenges
Nothing worth discussing.
